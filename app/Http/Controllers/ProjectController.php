<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return response()->json(Auth::user()->products);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validated = $this->validate($request, [
            'nama_product' => 'required|max:255',
            'harga' => 'required|max:255',
        ]);
        $produk = new Product($validated);
        $produk->user_id = Auth::user()->id;
        $produk->save();
        return response()->json($produk);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product, $id)
    {
        //
        $product = Product::where('id', $id)->where('user_id', Auth::user()->id)->first();
        if (empty($product)){
            abort(404, "Data Tidak Ditemukan");
        }
        return response()->json($product);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product, $id)
    {
        //
        $product = Product::where('id', $id)->where('user_id', Auth::user()->id)->first();
        if (empty($product)){
            abort(404, "Data Tidak Ditemukan");
        }
        $validated = $this->validate($request, [
            'nama_product' => 'required|max:255',
            'harga' => 'required|max:255',
        ]);
        $product->title = $validated['nama_product'];
        $product->description = $validated['harga'];
        $product->save();
        return response()->json($product);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product, $id)
    {
        //
        $product = Product::where('id', $id)->where('user_id', Auth::user()->id)->first();
        if (empty($product)){
            abort(404, "Data Tidak Ditemukan");
        }
        $product->delete();
        return response()->json(['message' => 'Data Terhapus']);
    }
}